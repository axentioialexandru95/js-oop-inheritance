var BaseClass = function()
{
    this.name = "I'm a base class"
}

BaseClass.prototype.getName = function()
{
    return this.name;
}

BaseClass.prototype.setName = function(str)
{
    this.name = str;
    return this.name;
}

var base = new BaseClass();

function extend(ChildClass, ParentClass)
{
    ChildClass.prototype = new ParentClass();
    ChildClass.prototype.constructor = ChildClass;
}

var SubClass1 = function()
{
    this.setName("I'm subclass1");
}

var SubClass2 = function()
{
    this.setName("I'm subclass2");
}

extend(SubClass1, BaseClass);
extend(SubClass2, BaseClass);

var base = new BaseClass();
var sub1 = new SubClass1();
var sub2 = new SubClass2();

alert(base.getName());
alert(sub1.getName());
alert(sub2.getName());